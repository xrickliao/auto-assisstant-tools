import urllib.request as req
from tqdm import tqdm
import numpy as np
from bs4 import BeautifulSoup as soup
import os

"""
to-do list:
1. give a url and specific file type, get all files of this type.
   ref: https://gist.github.com/mgnisia/d6b479dd8e58764a2ace2c99c8139181
   file:batch_pdf.py
"""

def get_file_from_url_with_console_output(url, target):
	# file_name = url.split('/')[-1]
	u = req.urlopen(url)
	f = open(target, 'wb')
	# meta = u.info()
	# print(u.headers)
	file_size = int(u.headers["Content-Length"])
	print("Downloading: {} Bytes: {}".format(file_name, file_size))
	file_size_dl = 0
	block_sz = 8192
	progressNum = np.ceil(file_size/block_sz)
	while True:
	    buffer = u.read(block_sz)
	    if not buffer:
	        break
	    file_size_dl += len(buffer)
	    f.write(buffer)
	    status = r"%10d  [%3.2f%%]".format(file_size_dl, file_size_dl * 100. / file_size)
	    status = status + chr(8)*(len(status)+1)
	    print(status)
	f.close()

def get_file_from_url_with_pbar(url, target):
	# file_name = url.split('/')[-1]
	u = req.urlopen(url)
	f = open(target, 'wb')
	# meta = u.info()
	file_size = int(u.headers["Content-Length"])
	print("Downloading: {} Bytes: {}".format(file_name, file_size))
	file_size_dl = 0
	block_sz = 8192
	progressNum = int(np.ceil(file_size/block_sz))
	for _ in tqdm(range(progressNum)):
	    buffer = u.read(block_sz)
	    if not buffer:
	        break
	    file_size_dl += len(buffer)
	    f.write(buffer)
	f.close()

if __name__ == "__main__":
	url_ = "https://www.ee.columbia.edu/~stanchen/spring16/e6870/slides/lecture2.pdf"
	file_name = url_.split('/')[-1]
	target_ = "./{}".format(file_name)
	get_file_from_url_with_pbar(url_, target_)